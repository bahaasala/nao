<?php
    if (isset ($_GET['page']))
    {
        $page = $_GET['page'];
    } else {
        $page='move';
    }
?>

<!DOCTYPE html>
<html>
    <head>
       <link rel="stylesheet" type="text/css" href="style.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="style-gallery.css">
    <!--===============================================================================================-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--===============================================================================================-->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!--===============================================================================================-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!--===============================================================================================-->
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous">
        </script>
    <!--===============================================================================================-->
        <script src="arrows.js"></script>
    <!--===============================================================================================-->     
    </head>  
    <body>
        <div class="header">
            <div class="logo">
                <a href="index.php?page=move"><img class="icon" src="images/nao-icon.png" alt="icon"><img src="images/logo.png" alt="logo"></a>
            </div>
        </div>

        <?php include 'includes/navbar.inc.php'; ?>
        <?php include 'includes/'.$page.'.inc.php'; ?>
        
        <div class="footer opacity">
            <strong><p>© NAO. All rights reserved</p></strong>
        </div>
    </body>
</html>