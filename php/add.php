<?php 
    include ('database-config.php');
    
    $name = (isset($_POST['name'])) ? $_POST['name'] : die ("error");
    $story = (isset($_POST['story'])) ? $_POST ['story'] : die ("error");

    $stmt = $conn->prepare("INSERT INTO `patients` (`name`, `story`) VALUES (:name, :story);");
    $stmt->bindParam(':name', $name, PDO::PARAM_STR);
    $stmt->bindParam(':story', $story, PDO::PARAM_STR);
    $stmt->execute();
    
    header("Location: ../index.php?page=add");

?>
    