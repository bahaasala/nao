var goBackLeft = goBackRight = goBackUp = goBackDown = false;
var stopLeft = stopRight = stopUp = stopDown = false;


$(document).ready(function() {
  $(".fa-arrow-circle-left").click(function() {
    var arrow = $(this);
    setInterval(function() {
      currentpos = $(".fa-arrow-circle-left").css("right").replace("px", "")
      if (currentpos >= 25) goBackLeft = true;
      if (currentpos <= 0) goBackLeft = false;
      if (goBackLeft) newpos = parseFloat(currentpos) - parseFloat(1);
      else newpos = parseFloat(currentpos) + parseFloat(1);
      arrow.css("right", newpos+"px");
    }, 10);
  });

  $(".fa-arrow-circle-right").click(function() {
    var arrow = $(this);
    setInterval(function() {
      currentpos = $(".fa-arrow-circle-right").css("left").replace("px", "")
      if (currentpos >= 25) goBackRight = true;
      if (currentpos <= 0) goBackRight = false;
      if (goBackRight) newpos = parseFloat(currentpos) - parseFloat(1);
      else newpos = parseFloat(currentpos) + parseFloat(1);
      arrow.css("left", newpos+"px");
    }, 10);
  });

  $(".fa-arrow-circle-up").click(function() {
    var arrow = $(this);
    setInterval(function() {
      currentpos = $(".fa-arrow-circle-up").css("bottom").replace("px", "")
      if (currentpos >= 20) goBackUp = true;
      if (currentpos <= 0) goBackUp = false;
      if (goBackUp) newpos = parseFloat(currentpos) - parseFloat(1);
      else newpos = parseFloat(currentpos) + parseFloat(1);
      arrow.css("bottom", newpos+"px");
    }, 10);
  });

  $(".fa-arrow-circle-down").click(function() {
    var arrow = $(this);
    setInterval(function() {
      currentpos = $(".fa-arrow-circle-down").css("top").replace("px", "")
      if (currentpos >= 20) goBackDown = true;
      if (currentpos <= 0) goBackDown = false;
      if (goBackDown) newpos = parseFloat(currentpos) - parseFloat(1);
      else newpos = parseFloat(currentpos) + parseFloat(1);
      arrow.css("top", newpos+"px");
    }, 10);
  });

    
    $("#forward").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            forward: "forward"
        });
    });
    $("#backwards").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            backwards: "backwards"
        });
    });
    $("#right").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            right: "right"
        });
    });
    $("#left").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            left: "left"
        });
    });
    $("#stop").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            stop: "stop"
        });
    });
    $("#sit").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            sit: "sit"
        });
    });
    $("#stand").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            stand: "stand"
        });
    });
    $("#laydown").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            laydown: "laydown"
        });
    });
    $("#footsteps").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            footsteps: "footsteps"
        });
    });
    $("#movearmsf").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            movearmsf: "movearmsf"
        });
    });
    $("#speak_info").click(function() {
        $.post("/LE-ICT-AO-NAO/includes/move.inc.php", {
            speak_info: "speak_info"
        });
    });
    QiSession(function (session) {
          console.log("connected!");
          // you can now use your QiSession
        }, function () {
          console.log("disconnected");
        });
});
