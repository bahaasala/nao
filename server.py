import sys
import socket
import argparse
import motion
import almath
import time
from naoqi import ALProxy
import json
from threading import Thread
import mysql.connector

robot_ip_addr = "192.168.1.108"
robot_port = 9559
asr = ALProxy("ALSpeechRecognition", robot_ip_addr, robot_port)

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket_addr = ('0.0.0.0', 1030)
socket.bind(socket_addr)
socket.listen(5)
print('Server listening on port 1030')

def listenForNames():
    print("Listening for names to be said by humans...")
    data = known_names = []
    last_time = count = 0
    last_name = ""
    
    asr = ALProxy("ALSpeechRecognition", robot_ip_addr, robot_port)
    asr.pause(True)
    asr.setLanguage("English")

    dbh = mysql.connector.connect(host="localhost", user="root", passwd="", database="nao")
    stmt = dbh.cursor(prepared=True)
    stmt.execute("SELECT name FROM patients");
    names = stmt.fetchall()
    for name in names:
        known_names.append(str(name[0]))

    asr.setVocabulary(known_names, False)
    asr.subscribe(robot_ip_addr)
    
    memProxy = ALProxy("ALMemory", robot_ip_addr, robot_port)
    memProxy.subscribeToEvent("WordRecognized", robot_ip_addr, "wordRecognized")
    
    posProxy = ALProxy("ALRobotPosture", robot_ip_addr, robot_port)
    posProxy.goToPosture("Stand", 1.0)
    asr.pause(False)

    while True:
        time.sleep(0.5)
        data = memProxy.getData("WordRecognized")
        if data[1] != last_time and data[0] != last_name:
            last_time = data[1]
            last_name = data[0]
            count = count + 1
            if count == 1:
                lastname = ""
                continue
            
            stmt = dbh.cursor(prepared=True)
            stmt.execute("SELECT story FROM patients WHERE name = %s", (data[0],))
            story = stmt.fetchone()[0]
            tts = ALProxy("ALTextToSpeech", robot_ip_addr, robot_port)
            tts.say(str(story))
            
listenThread = Thread(target = listenForNames, args = ())
listenThread.start()

def handlePacket(data, connection):
    command = json.loads(data)
    asr.pause(True)
    if command["action"] == "talk":
        tts = ALProxy("ALTextToSpeech", robot_ip_addr, robot_port)
        tts.setVolume(1)
        tts.say(str(command["text"]))
        
    if command["action"] == "speak_info":
        tts = ALProxy("ALTextToSpeech", robot_ip_addr, robot_port)
        tts.say("Nao (now pronounced now) is an autonomous, programmable humanoid robot developed by Aldebaran Robotics, a French robotics company headquartered in Paris, which was acquired by SoftBank Group in 2015 and rebranded as SoftBank Robotics. The robot's development began with the launch of Project Nao in 2004. On 15 August 2007, Nao replaced Sony's robot dog Aibo as the robot used in the RoboCup Standard Platform League (SPL), an international robot soccer competition. The Nao was used in RoboCup 2008 and 2009, and the NaoV3R was chosen as the platform for the SPL at RoboCup 2010")


    if command["action"] == "move":
        pos = ALProxy("ALRobotPosture", robot_ip_addr, robot_port)
        pos.goToPosture("Stand", 0.5)
        motion = ALProxy("ALMotion", robot_ip_addr, robot_port)
        if command["position"] == "forward":
            motion.moveTo(1.0, 0.0, 0.0)
        elif command["position"] == "left":
            motion.moveTo(0.0, 1.0, 0.0)
        elif command["position"] == "stop":
            motion.stopMove()
        elif command["position"] == "right":
            motion.moveTo(0.0, -1.0, 0.0)
        elif command["position"] == "backwards":
            motion.moveTo(-1.0, 0.0, 0.0)
            

            
        else:
            tts = ALProxy("ALTextToSpeech", robot_ip_addr, robot_port)
            tts.setVolume(1)
            tts.say("Invalid robot position. Got {} which is not a valid command".format(command["position"]))
            
    if command["action"] == "pose":
        pos = ALProxy("ALRobotPosture", robot_ip_addr, robot_port)
        if command["pose"] == "sit":
            pos.goToPosture("Sit", 0.5)
        elif command["pose"] == "stand":
            pos.goToPosture("Stand", 0.5)
        elif command["pose"] == "laydown":
            pos.goToPosture("LyingBack", 0.5)
        elif command["pose"] == "movearmsf":
            pos.goToPosture("StandZero", 0.5)
            
    if command["action"] == "footsteps":
        motionProxy  = ALProxy("ALMotion", robot_ip_addr, robot_port)
        postureProxy = ALProxy("ALRobotPosture", robot_ip_addr, robot_port)
        
        # Wake up robot
        motionProxy.wakeUp()

        # Send robot to Stand Init
        postureProxy.goToPosture("StandInit", 0.5)
        
        ###############################
        # First we defined each step
        ###############################
        footStepsList = []

        # 1) Step forward with your left foot
        footStepsList.append([["LLeg"], [[0.06, 0.1, 0.0]]])

        # 2) Sidestep to the left with your left foot
        footStepsList.append([["LLeg"], [[0.00, 0.16, 0.0]]])

        # 3) Move your right foot to your left foot
        footStepsList.append([["RLeg"], [[0.00, -0.1, 0.0]]])

        # 4) Sidestep to the left with your left foot
        footStepsList.append([["LLeg"], [[0.00, 0.16, 0.0]]])

        # 5) Step backward & left with your right foot
        footStepsList.append([["RLeg"], [[-0.04, -0.1, 0.0]]])

        # 6)Step forward & right with your right foot
        footStepsList.append([["RLeg"], [[0.00, -0.16, 0.0]]])

        # 7) Move your left foot to your right foot
        footStepsList.append([["LLeg"], [[0.00, 0.1, 0.0]]])

        # 8) Sidestep to the right with your right foot
        footStepsList.append([["RLeg"], [[0.00, -0.16, 0.0]]])

        ###############################
        # Send Foot step
        ###############################
        stepFrequency = 0.8
        clearExisting = False
        nbStepDance = 2 # defined the number of cycle to make

        for j in range( nbStepDance ):
            for i in range( len(footStepsList) ):
                try:
                    motionProxy.setFootStepsWithSpeed(
                        footStepsList[i][0],
                        footStepsList[i][1],
                        [stepFrequency],
                        clearExisting)
                except Exception, errorMsg:
                    print str(errorMsg)
                    print "This example is not allowed on this robot."
                    exit()


        motionProxy.waitUntilMoveIsFinished()

        
    asr.pause(False)
        
def handleClient(connection):
    try:
        data = connection.recv(1024)
        if data:
            handlePacket(data, connection)
    except:
        connection.close()

while True:
    connection, client_addr = socket.accept()
    clientThread = Thread(target = handleClient, args = (connection, ))
    clientThread.start()
