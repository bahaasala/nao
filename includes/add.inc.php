<?php
    if (!isset($_SESSION['sess_userrole'])) {
            header("Location: index.php");
            exit;
    }
?>
<div class="container-add">
    <form class="w3-container" action="./php/add.php" name="add" method="Post">
        
        <h1>Add patient</h1>
        <hr>
        <p>
            <label>Name:</label>
            <input class="w3-input" type="text" name="name" autocomplete="off" required/>
        </p>
        <p>
            <label>Story:</label><br />
            <textarea class="textarea-add" type="text" name="story" required></textarea>
        </p>
        <button class="button buttonAdd" type="submit" name="add" value="Add">Add</button>
    </form>
</div>
