<?php
    include_once $_SERVER['DOCUMENT_ROOT'].'/LE-ICT-AO-NAO/connection.php';
    if (isset($_POST['forward'])) socket::sendData(json_encode(array("action" => "move", "position" => "forward")));
    if (isset($_POST['backwards'])) socket::sendData(json_encode(array("action" => "move", "position" => "backwards")));
    if (isset($_POST['right'])) socket::sendData(json_encode(array("action" => "move", "position" => "right")));
    if (isset($_POST['left'])) socket::sendData(json_encode(array("action" => "move", "position" => "left")));
    if (isset($_POST['stop'])) socket::sendData(json_encode(array("action" => "move", "position" => "stop")));
    if (isset($_POST['sit'])) socket::sendData(json_encode(array("action" => "pose", "pose" => "sit")));
    if (isset($_POST['stand'])) socket::sendData(json_encode(array("action" => "pose", "pose" => "stand")));
    if (isset($_POST['laydown'])) socket::sendData(json_encode(array("action" => "pose", "pose" => "laydown")));
    if (isset($_POST['movearmsf'])) socket::sendData(json_encode(array("action" => "pose", "pose" => "movearmsf")));
    if (isset($_POST['footsteps'])) socket::sendData(json_encode(array("action" => "footsteps")));
    if (isset($_POST['speak_info'])) socket::sendData(json_encode(array("action" => "speak_info")));
    if (isset($_POST['arms'])) socket::sendData(json_encode(array("action" => "arms")));
?>
<div id="mySidenav" class="sidenav">
    <div class="nao-move-pic">
        <img src="./images/nao-move.png"/>
    </div>
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="arrows">
        <h1>Move around</h1>
        <p><i name="forward" id="forward" class="fas fa-arrow-circle-up"></i></p>
        <i type="submit" name="left" id="left" class="fas fa-arrow-circle-left"></i>
        <div class="tooltip">
            <i name="stop" id="stop" class="fas fa-stop-circle"></i>
            <span class="tooltiptext">Stop</span>
        </div>
        <i name="right" id="right" class="fas fa-arrow-circle-right"></i>
        <p><i style="margin-bottom: 144px;" name="backwards" id="backwards" class="fas fa-arrow-circle-down"></i></p>
    </div>
</div>
<span id="sidebar1" onclick="openNav()">&#187; Move NAO</span>
<div class="columns">
    <div class="back">
        <h1>The functions:</h1>
        <p>Click on these buttons to see NAO's skills :)</p>
        <div class="tooltip">
            <button class="button button1" type="submit" id="sit">Sit</button>
            <span class="tooltiptext">The robot is going to sit down.</span>
        </div>
        <div class="tooltip">
            <button class="button button1" type="submit" id="stand">Stand</button>
            <span class="tooltiptext">The robot is going to stand up.</span>
        </div>
        <div class="tooltip">
            <button class="button button1" type="submit" id="laydown">Laydown</button>
            <span class="tooltiptext">Tooltip text</span>
        </div>
        <div class="tooltip">
            <button class="button button1" type="submit" id="footsteps">Footsteps</button>
            <span class="tooltiptext">Tooltip text</span>
        </div>
        <div class="tooltip">
            <button class="button button1" type="submit" id="movearmsf">Move Arms</button>
            <span class="tooltiptext">Tooltip text</span>
        </div>
    </div>
    <div class="back-info">
        <h1>What is NAO?!:</h1>
        <p>Let the robot tell you!</p>
        <p style="font-size: 50px;">&#8595;</p>
        <button class="button button-info" type="submit" id="speak_info">Info</button>
    </div>
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "60%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>