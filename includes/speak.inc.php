<?php
    if (isset($_POST['text'])) {
        include_once $_SERVER['DOCUMENT_ROOT'].'/LE-ICT-AO-NAO/connection.php';
		$data = array(
			"action" => "talk",
			"text" => $_POST['text']
		);
		socket::sendData(json_encode($data));
	}
?>

<div class="balk1">
    <form method="post">
        <h2>Type some text:</h2>
        <textarea placeholder="Type here:" class="textarea-speak" name="text" required></textarea>
        <p><button class="button buttonSpeak" type="submit">Speak</button></p>
    </form>
</div>
