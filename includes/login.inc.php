<form name="login" action="php/login.php" method="Post">
    <div class="container">
        <label for="uname">
            <b>Username</b>
        </label>
        <input type="text" name="username" value="" placeholder="Your username" autocomplete="off" required>
        <label for="psw">
            <b>Password</b>
        </label>
        <input type="password" name="password" value="" placeholder="Your password" required>
        <button class="loginbutton" type="submit" name="submit" value="Submit">Login</button>
    </div>
</form>