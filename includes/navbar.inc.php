<?php
$menuItems = array(
            array('move', 'Home'),
            array('speak', 'Speak'),
            array('images', 'Gallery')
            );

$menuItemsUsers = array(
            array('move', 'Home'),
            array('speak', 'Speak'),
            array('images', 'Gallery'),
            array('patients', 'Patients')
            );

$menuItemsAdmin = array(
            array('move', 'Move'),
            array('speak', 'Speak'),
            array('images', 'Gallery'),
            array('patients', 'Patients'),
            array('add', 'Add')
            );
?>

<ul>

<?php
    session_start();
    if (isset($_SESSION['sess_userrole']))
    {
        if($_SESSION['sess_userrole'] == "admin")
        {
            foreach($menuItemsAdmin as $menuItem)
            {
                echo '<li><a href="index.php?page=' . $menuItem[0] . '" ' . (strtolower($menuItem[0]) == strtolower($page) ? 'class="active"' : '') . '>' . $menuItem[1].'</a></li>';
            }
        }
    else if ($_SESSION['sess_userrole'] == "user")
        {
            foreach($menuItemsUsers as $menuItem)
            {
                echo '<li><a href="index.php?page=' . $menuItem[0] . '" ' . (strtolower($menuItem[0]) == strtolower($page) ? 'class="active"' : '') . '>' . $menuItem[1].'</a></li>';
            }
        }
    }else{
        foreach($menuItems as $menuItem)
            {
                echo '<li><a href="index.php?page=' . $menuItem[0] . '" ' . (strtolower($menuItem[0]) == strtolower($page) ? 'class="active"' : '') . '>' . $menuItem[1].'</a></li>';
            }
    }
?>
<?php if (isset($_SESSION['sess_userrole'])): ?>

<form action="php/logout.php">
    <input style="float:right" class="loginbutton" type="submit" value="Logout"/>
</form>
<?php endif; ?>
<?php if (!isset($_SESSION['sess_userrole'])): ?>
    <li style="float:right">
        <button class="loginbutton" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Login</button>
    </li>
<?php endif; ?>

<div id="id01" class="modal">
    <form class="modal-content animate" name="login" action="php/login.php" method="Post">
        <div class="imgcontainer">
            <img src="./images/img_avatar2.png" alt="Avatar" class="avatar">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div class="container input-login">
            <label for="uname">
                <b>Username</b>
            </label>

            <input type="text" name="username" value="" placeholder="Your username" autocomplete="off" required>
            <label for="psw"><b>Password</b></label>
            <input type="password" name="password" value="" placeholder="Your password" required>

            <button class="button buttonlogin" type="submit" name="submit" value="Submit">Login</button>
        </div>
    </form>
</div>
</ul>


<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
